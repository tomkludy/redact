var clarinet = require("clarinet")
  , stream = require('stream')
  , fs = require('fs')
  , digestStream = require('digest-stream')
  , parser = clarinet.createStream()
  , context = [{}]  // must start with an empty context to handle root node
  ;

// Set up the output stream.
var rs = new stream.Readable;
var writeRS = function (s) {
  rs.push(s);
}

// The stream will try to "pull" from us, but we don't operate that way.
// Ignore the pulls, we push.
rs._read = function () {};

// Direct our output to a file, but calculate the md5 digest on the way.
var dstream = digestStream('md5','hex',function(digest) {
  console.error("md5 hex digest: " + digest);
});
rs.pipe(dstream).pipe(fs.createWriteStream("output.json"));

// List of regular expressions matching keys where we want to redact
// both the key name, and all contents within the key.
// In this toy example, I don't want anyone to know that images contain
// points, or what their shape is, or of the existence of "solid"s.
var redactKeys = ["^.*\\.image\\[\\d*\\]\\.points.*"
                 ,"^.*\\.image\\[\\d*\\]\\.shape.*"
                 ,"^.*\\.solid.*"
                 ];

// List of regular expressions matching keys where we want to redact
// just the value contained within the key, but keep the key name.
// In this toy example, I don't want any "stroke" value to be known.
var redactKeyValues = ["^.*\\.stroke$"];

// List of regular expressions matching values where we want to redact
// the value regardless of which key(s) it is in.
// In this toy example, I don't want any email addresses to be known.
// The email addresses are assumed to be (1 or more non-@ characters) followed
// by an '@' sign, followed by more non-@ characters, with a dot (.) and 2 thru
// 4 letters at the end.
var redactValues = ["^[^@]+@[^@]+\\.[a-zA-Z]{2,4}$"];

// Set the parser state to redact values in the current context
var setRedact = function (redact) {
  var ctx = context[context.length - 1];
  ctx.redact = redact;
}

// Find out whether values in the current context should be redacted
var getRedact = function () {
  var ctx = context[context.length - 1];
  return ctx.redact;
}

// Add a context to the stack, passing the redaction state to it.
var createContext = function (newCtx) {
  // If the parent context was a redacted value, then the child context must
  // redact also.
  var ctx = context[context.length - 1];
  newCtx.redact = newCtx.redact || ctx.redact;
  context.push(newCtx);

  // If the newly added key is supposed to be completely redacted, do it.
  var path = getPathStr();
  var redactKey = shouldRedact(redactKeys, path);
  if (redactKey) {
    newCtx.redact = true;
  }
}

// Returns true if the 'test' array contains any regular expression
// that matches the given value.
var shouldRedact = function (test, value) {
  if (!value) {
    return false;
  }
  // The value could be a string, bool, or number.  Convert to string for regex match.
  var v = value.toString();
  return test.some(function (currentValue) {
    return v.match(currentValue);
  });
}

// Output a string if we are not in a redact context, prefixed by a comma
// if we are in an array context and this is not the 0th element.
var handleArrayContext = function(output) {
  if (getRedact()) {
    return;
  }
  var ctx = context[context.length - 1];
  var needComma = ctx.isArray && ctx.elemNo > 0;
  writeRS((needComma ? "," : "") + output);
}

// Increment the element number in the parent array context.
var incrArrayContext = function() {
  var ctx = context[context.length - 1];
  if (ctx.isArray) {
    ctx.elemNo ++;
  }
}

// Convert the context into a string in a predictable way, so that
// regular expressions can be used to find redact-able items.
var getPathStr = function () {
  return context.reduce(function (previousValue, currentValue) {
    if (currentValue.isArray) {
      return previousValue + "[" + currentValue.elemNo + "]";
    } else {
      return previousValue + (previousValue ? "." : "") + (currentValue.name || "");
    }
  }, "");
}

// Debug output.  This can help if you are trying to formulate regular expressions
// to decide what to redact.
var dbgOutput = function (value) {
  var pathStr = getPathStr();
  console.info(pathStr + " = " + value);
}

// Sanitize the value (and all of the value's children) if we are writing in a context
// where the value should be redacted.
var safeValue = function () {
  var path = getPathStr();
  if (shouldRedact(redactKeyValues, path)) {
    handleArrayContext(JSON.stringify("[redacted]"));
    setRedact(true);
  }
}

// Write a key (object property name).
var writeKey = function (key, prevCtx) {
  if (!getRedact()) {
    if (prevCtx && !prevCtx.nextIsFirst) {
      writeRS(",");
    }
    writeRS(key ? (JSON.stringify(key) + ":") : "");
  } else {
    // Need to keep track of which key is going to be the first one output, in
    // case one or more at the beginning are redacted.
    context[context.length - 1].nextIsFirst = (!prevCtx) || (prevCtx.nextIsFirst);
  }
}

// Write the closing syntax for an object or array, unless the object or array was redacted.
var writeClose = function (str) {
  if (!getRedact()) {
    writeRS(str);
  }
}

parser.on("error", function (e) {
  // an error happened. e is the error.
  console.error("error!", e);
  this._parser.error = null;
  this._parser.resume();
});

parser.on("value", function (v) {
  // got some value.  v is the value. can be string, double, bool, or null.
  dbgOutput(v);
  if (shouldRedact(redactValues, v)) {
    v = "[redacted]";
  }
  handleArrayContext(JSON.stringify(v));
  incrArrayContext();
});

parser.on("openobject", function (key) {
  // opened an object. key is the first key.
  handleArrayContext("{");
  createContext({name:key || ""});  // start a nested context that is not an array
  writeKey(key);
  safeValue();
});

parser.on("key", function (key) {
  // got a key in an object.
  var oldCtx = context.pop();
  createContext({name:key || ""});  // start a nested context that is not an array
  writeKey(key, oldCtx);
  safeValue();
});

parser.on("closeobject", function () {
  // closed an object.
  context.pop(); // exit the object context
  writeClose("}");
  incrArrayContext();
});

parser.on("openarray", function () {
  // opened an array.
  handleArrayContext("[");
  createContext({isArray:true,elemNo:0}); // start the array context
});

parser.on("closearray", function () {
  // closed an array.
  context.pop(); // end the array context
  writeClose("]");
  incrArrayContext();
});

parser.on("end", function () {
  // parser stream is done, and ready to have more stuff written to it.
  writeRS(null);
});


// Now, hook up the input file and stream it into clarinet's parser.
fs.createReadStream("sample.json")
  .pipe(parser);
